import cv2 as cv
import numpy as np
import os
from PIL import Image
import pandas as pd

	

directory_raw = './raw/after_4'
directory_tif = './tif_files'
directory_test = './test_data'


def rgb_to_hex(r, g, b):
    """
		Возвращает цвет RGB (255, 255, 255) в формате HEX (#ffffff)
	"""
    return '#{:02x}{:02x}{:02x}'.format(r, g, b)


def raw_to_tif(directory_raw: str):
	"""
		Функция перевода файла из формата raw в tiff
	"""

	def export_entry(entry):
		name_dir = entry.path.replace('/', '_').replace('а', 'a').replace('б', 'b')[1:]
		if entry.is_file() and entry.name.endswith('.raw'): # Проверка, что переданный путь к файлу
			print(entry.path, entry.is_file())
			rows = 1024 # количество столбцов в картинке
			cols = 1024 # количество строк в картинке
			f = np.fromfile(entry.path, dtype = '<H',count=cols*rows) # считывание файла, формата int16, с размером cols*rows
			im = f.reshape((rows, cols)) #notice row, column format # преобразование файла в изображение рамерами cols*rows
			cv.imwrite(f'tif_files/{name_dir}_{entry.name}.tif', im) # сохранение изображения в формате tif (RGB, 8-бит (потеря информации при уменьшении разрядности пикселя))
 
	for entry in os.scandir(directory_raw): # Сканирование директории и поиск raw файлов или вложеных папок
		if entry.is_dir():
			for entry_x in os.scandir(entry.path):
				export_entry(entry_x)
		elif entry.is_file:
			print(entry.name)
			export_entry(entry)



def find_average_pix(directory_tif=None, file_name=None, image=None):
	"""
		Функция поиска среднего цвета пикселя на изображении. Передается либо путь до изображения, либо само изображение
	"""

	if image is None:
		image = Image.open(directory_tif + '/' + file_name) # чтение изображения из файла
	else:
		image = Image.fromarray(image) # иначе преобразовать изображение из np.array в объект Image
	image = image.convert('RGB')
	
	w, h = image.size # Определение размеров изображение
	rr, gg, bb = 0, 0, 0  # Обнуление счетчиков
	

	# Цикл, который перебирает каждый элемент np.array
	for x in range(w):
		for y in range(h):
			r, g, b = image.getpixel((x, y)) # Получение пикселя и его цветов
			rr += r
			gg += g
			bb += b
 
	# Вычисление среднего цвета пикселя
	cnt = w * h 
	col = (rr // cnt, gg // cnt, bb // cnt) 

	col_hex = rgb_to_hex(*col) # Вызов функции перевода цвета в HEX формат

	print(f'Average color: RGB {col} | HEX: {col_hex}')

	return col, col_hex

def find_coil(directory_tif: str, file_name: str):
	"""
		Функция поиска катушки на изображении, минимизации шумов и позиционирования
	"""

	image = cv.imread(directory_tif + '/' + file_name) # Читаем изображение
	image_gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY) # Преобразуем в оттенки серого

	ret,thresh = cv.threshold(image_gray,90,255,cv.THRESH_TOZERO) # Преобразуем изображение с помощью фильтра, где все пиксели темнее 90 станут черными, остальные белые
	contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE) # Функция поиска контуров
	cont_count = 0
	angle = 0
	box_coords = None

	# В цикле идем по всем найденным контурам, пытаемся найти нужный а также посчитать матрицу поворота для позиционирования катушки
	for cnt in contours:
		rect = cv.minAreaRect(cnt) # пытаемся вписать прямоугольник
		box = cv.boxPoints(rect) # поиск четырех вершин прямоугольника
		box = np.intp(box) # округление координат
		area = int(rect[1][0]*rect[1][1]) # вычисление площади
		if area > 130_000:
			cont_count += 1
			angle = rect[2] + 90

			# Create a rotation matrix using the angle
			rows, cols = image.shape[:2]
			M = cv.getRotationMatrix2D((cols/2, rows/2), angle, 1)

			box_coords = cv.transform(np.array([box]), M)[0]
			box_coords = np.int0(box_coords)


	if cont_count == 1: # Если нашли катушку
		rotated = cv.warpAffine(image, M, (cols, rows)) # Поворачиваем изображение
		mask = np.zeros_like(image) # создаем маску

		# Далее идет вписывание катушки на новый черный фон

		# Fill the box with white color on the mask
		cv.fillPoly(mask, [box_coords], (255, 255, 255))

		# Extract the region of interest (ROI) from the original image using the bounding box of the rectangle
		x, y, w, h = cv.boundingRect(box_coords)
		roi = rotated[y:y+h, x:x+w]

		# Copy the ROI onto the black image
		mask[y:y+h, x:x+w] = roi
		return(mask)
	
	elif cont_count == 0:
		print('Контур на изображении', directory_tif + '/' + file_name, 'не был найден')

	elif cont_count > 1:
		print('Найдено больше 1 контура', directory_tif + '/' + file_name)


def run_scan():

	"""
	Функция подсчета коэффициента K'
	"""

	dir = directory_tif # рабочая директоря
	# Словарь для записи новых данных
	d = {
			'image_path': list(),
			'image_name': list(),
			'slice': list(),
			'rgb': list(),
			'hex': list(),
			'K_MED': list(),
			'K_SA': list()
		}
	for entry in os.scandir(dir):
		if entry.is_file() and entry.name.endswith('.tif'): # Запускаем функцию find_coil на всех tiff в dir
			try:
				img = find_coil(dir, entry.name)
			except Exception as e:
				print(e)
			print(entry.name, ':')
			try:
				color, color_hex = find_average_pix(image=img) # Поиск среднего пикселя

				# Запись данных в словарь
				d['image_path'].append(entry.path)
				d['image_name'].append(entry.name)
				d['slice'].append('hui')
				d['hex'].append(color_hex)
				d['rgb'].append(str(color))

				color_dec = int(color_hex[1:], 16)

				# Подсчет коэффициентов с К медианным и средним
				K_med = color_dec / 6339605.938
				K_SA = color_dec / 6163688.558

				# Запись данных в словарь
				d['K_MED'].append(K_med)
				d['K_SA'].append(K_SA)

				# Добавление информации на изображение
				cv.putText(img, f'Average color: RGB {color} | HEX: {color_hex} | K= {str(K_SA)}', (0, 40), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv.LINE_AA)
				cv.putText(img, entry.path, (0, 80), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv.LINE_AA)
				
				# Сохранение изоражений
				cv.imwrite(f'result/{entry.name}', img)
			except Exception as e:
				print(e)
			print()

	# Генерация талицы с данными и сохранение в эксель
	df = pd.DataFrame(d)
	df.to_excel('data.xlsx')

if __name__=='__main__':
	""" Здесь точка входа в программу, последовательно запускаются функции. Их содержимое смотреть выше """
	raw_to_tif(directory_raw)
	run_scan()